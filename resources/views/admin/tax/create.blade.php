@extends('layouts.admin')
@section('content')
<div class="container mmt mb-5">
  <div class="row mb-5">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h3 class="text-center title-2 font-weight-bold"> Advance Tax</h3>
        </div>
        <div class="card-body">
          <form action="/admin/advance-tax.php" method="post">
            <div class="form-row">
              <div class="form-group col-md-6">
                <label for="inputName">Name</label>
                <input type="text" class="form-control" id="inputName" placeholder="Name" name="name" required="true">
              </div>
              <div class="form-group col-md-6">
                <label for="inputAmount">Amount</label>
                <input type="number" class="form-control" id="inputAmount" placeholder="Amount" name="amount" required="true">
              </div>
            </div>
            <div class="form-row">
              <div class="form-group col-md-12">
                <label for="inputDate">Date</label>
                <div class="input-group date datepicker" style="padding:0px;" data-date-format="dd-mm-yyyy">
                  <input class="form-control form-control-user date-form" type="text" name="date" placeholder="Due Date">
                  <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1"><i class="fa fa-calendar"></i></span>
                </div>
              </div>
            </div>
            <div class="row form-group">
              <div class="col-lg-12 col-md-8">
                <label for="select" class=" form-control-label">Description</label>
                <textarea class="form-control" id="comment" name="taxDescription" rows="3" placeholder="Type Here...."></textarea>
              </div>
            </div>
            <button type="submit" class="btn btn1" name="submit">Save</button>
          </form>
        </div>
      </div>
    </div>
  </div>

<div class="row m-auto">
  <div class="col-lg-12 m-auto">
    <h2 class="title-1 m-b-25">All Taxes</h2>
    <div class="table-responsive table--no-card m-b-40">
      <table class="table table-borderless table-striped table-earning">
        <thead class="btn1">
          <tr>
            <th>Name</th>
            <th>Amount</th>
            <th>Description</th>
            <th>Date</th>
            <th>Action</th>
          </tr>
        </thead>
        <tbody>
        </tbody>
      </table>
      <br><br>
    </div>
  </div>
</div>
</div>

@endsection