@extends('layouts.admin')
@section('content')
<div class="container mmt mb-5">
  <div class="card update-form">
    <div class="card-header">
      <h3 class="text-center title-2 font-weight-bold">Add Vehicle</h3>
    </div>
  </div>
  <div class="card">
    <div class="card-body mb-5">
      <form action="" method="post" novalidate="novalidate">
        <div class="form-group">
          <label class="control-label mb-1">Vehicle Number</label>
          <input type="text" class="form-control" aria-required="true" aria-invalid="false" name="vehicle-no" placeholder="Enter Your Vehicle Number" required="true">
          
        </div>
          <button id="button" type="submit" name="submit" class="btn btn1">
              <span class="d-flex ">Add Vehicle</span>
            </button>                                           
      </form>
    </div>
    <div class="card update-form">
      <div class="card-header">
        <h3 class="text-center title-2 font-weight-bold">Challan</h3>
      </div>
    </div><br>
    <div class="col-lg-12">
      <div class="table-responsive table--no-card m-b-40">
        <table class="table table-borderless table-striped table-earning">
          <thead class="btn1 pt-5" >
            <tr>
              <th>Vehicle No.</th>
              <th>Insurance Remaining Days</th>
              <th>Fitness Remaining Days</th>
              <th>Permitt Remaining Days</th>
              <th>Pollution Remaining Days</th>
              <th>Action</th>
            </tr>
          </thead>
          <tbody>
            <tr>
              <td>1</td>
              <td> Days</td>
              <td> Days</td>
              <td> Days</td>
              <td> Days</td>
              <td><button class="btn btn1"><a href="delete.php?id3=1" style="text-decoration: none;">Delete</a></button></td>
            </tr>
         
              <td>dsfsdf</td>
              <td>-1 Days</td>
              <td>9 Days</td>
              <td>0 Days</td>
              <td>8 Days</td>
              <td><button class="btn btn1"><a href="delete.php?id3=5" style="text-decoration: none;">Delete</a></button></td>
            </tr>
          </tbody>
        </table>
      </div>
      <br><br>
    </div>
  </div>
</div>
@endsection