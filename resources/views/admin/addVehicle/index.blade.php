@extends('layouts.admin')
@section('content')
    <div class="container mmt mb-5">
        <div class="row">
           <div class="col-lg-12">
               <div class="card update-form">
                    <div class="card-header">
                       <h3 class="text-center title-2"> Update Vehicle Data</h3>
                    </div>
                    <div class="card-body card-block">
                        <form id="form" action="/admin/update-data.php" method="post" class="form-horizontal">
                              <div class="row form-group">
                                <div class="col col-md-4">
                                    <label for="select" class=" form-control-label">Select Vehicle</label>
                                </div>
                                <div class="col-12 col-md-8">
                                    <select name="vehicle-no" id="select" class="form-control" onchange="get_value(this.value)" required="true">
                                      <option id="submitvehicle" selected="true">Choose Vehicle</option>
                                        <option id="submitvehicle" value="1">1</option><option id="submitvehicle" value="2">1401</option><option id="submitvehicle" value="4">UK07CC2099</option><option id="submitvehicle" value="5">dsfsdf</option>                                                    </select>
                                </div>
                              </div>
                            <div class="row">
                                <div class="col col-md-4">
                                    <label for="select" class=" form-control-label">Insurance</label>
                                </div>
                                <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group ">
                         <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form " type="text" name="insurance_start_date" placeholder="Start Date" required="">
                                        <span class="input-group-addon date-form-addon ml-1 align-items-center d-flex text-center"><i class="fa fa-calendar"></i></span>
                                    </div>                                         
                              </div>
                                </div>
                                                      <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                              
                           <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="insurance_end_date" placeholder="End Date" required="">
                                        <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1  "><i class="fa fa-calendar"></i></span>
                                    </div>  
                            </div>
                                </div>
                              </div>
                                <div class="row">
                                <div class="col col-md-4">
                                    <label for="select" class=" form-control-label">Fittness</label>
                                </div>
                                <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                         
                            <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="fitness_start_date" placeholder="Start Date" required="">
                                        <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1"><i class="fa fa-calendar"></i></span>
                                    </div>                                            </div>
                                </div>
                                 <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                              
                           <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="fitness_end_date" placeholder="End Date" required="">
                                        <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1"><i class="fa fa-calendar"></i></span>
                                    </div> 
                            </div>
                                </div>
                              </div>
                                <div class="row">
                                <div class="col col-md-4">
                                    <label for="select" class=" form-control-label">Permit</label>
                                </div>
                                <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                         
                            <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="permitt_start_date" placeholder="Start Date" required="">
                                        <span class="input-group-addon date-form-addonalign-items-center d-flex text-center ml-1"><i class="fa fa-calendar"></i></span>
                                    </div>                                             </div>
                                </div>
                                 <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                              
                           <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="permitt_end_date" placeholder="End Date" required="">
                                        <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1"><i class="fa fa-calendar"></i></span>
                                    </div>  
                            </div>
                                </div>
                              </div>
                                <div class="row form-group">
                                <div class="col col-md-4">
                                    <label for="select" class=" form-control-label">Pollutioin</label>
                                </div>
                                <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                         
                            <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="pollution_start_date" placeholder="Start Date" required="">
                                        <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1"><i class="fa fa-calendar"></i></span>
                                    </div>                                              </div>
                                </div>
                                 <div class="col-12 col-md-4 date-padding">
                                     <div class="form-group">
                              
                           <div class="input-group date datepicker" data-date-format="dd-mm-yyyy">
                                        <input class="form-control form-control-user date-form" type="text" name="pollution_end_date" placeholder="End Date" required="">
                                        <span class="input-group-addon date-form-addon align-items-center d-flex text-center ml-1 "><i class="fa fa-calendar"></i></span>
                                    </div>  
                            </div>
                                </div>
                              </div>
                                <button id="buttonsubmit" type="submit" name="submit" class="btn btn1">
                                    
                                    <span id="">SAVE</span>
                                   
                                </button><br><br>
                        </form>
                    </div>
             
                </div>
            </div>
                 </div>
  </div>
</div>

@endsection