@extends('layouts.admin')
@section('content')
<div class="container mmt mb-5">
  <div class="row">
    <div class="col-lg-12">
      <h2 class="title-1 m-b-25 font-weight-bold mb-5">Paid Challan</h2>
      <div class="table-responsive table--no-card m-b-40">
        <table class="table table-borderless table-striped table-earning">
          <thead class="btn1 ">
            <tr>
              <th>Vehicle No.</th>
              <th>Challan Date</th>
              <th>Challan Comment</th>
              <th>Challan Status</th>
              <th>Action</th>
              <th>Document</th>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
@endsection