@extends('layouts.admin')
@section('content')
<div class="container mmt mb-5">
  <div class="row">
    <div class="col-lg-12">
      <div class="card">
        <div class="card-header">
          <h3 class="text-center title-2  font-weight-bold"> Add Challan</h3>
        </div>
        <div class="card-body card-block">
          <form action="/admin/" method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="row form-group">
              <div class="col col-md-4">
                <label for="select" class=" form-control-label">Select Vehicle</label>
              </div>
              <div class="col-6 col-md-8">
                <select name="vehicle-id" id="select" class="form-control" required="true">
                  <option value="1">1</option>
                  <option value="2">1401</option>
                  <option value="4">UK07CC2099</option>
                  <option value="5">dsfsdf</option>
                </select>
              </div>
            </div>
            <div class="row form-group">
              <div class="col col-md-4">
                <label for="select" class=" form-control-label">Date Of Challan</label>
              </div>
              <div class="col-6 col-md-8">
                <label for="selefluidepicker" class="input-group date" data-date-format="dd-mm-yyyy">
                  <input class="form-control form-control-user date-form" type="text" name="challan_date" placeholder="Course Date">
                  <span class="input-group-addon date-form-addon align-items-center d-flex text-center pl-1"><i class="fa fa-calendar"></i></span>
              </div>
            </div>
            <div class="row form-group">
                <div class="col col-md-4">
                  <label for="select" class=" form-control-label">Comment</label>
                </div>
                <div class="col-6 col-md-8">
                    <textarea class="form-control" id="comment" name="challan_comment" rows="3" required="true"></textarea>
                </div>
            </div>

     
            <div class="row form-group">
              <div class="col col-md-4">
                <label for="select" class=" form-control-label">Upload Document</label>
              </div>
              <div class="col-12 col-md-8">
                <input type="file" name="image" required="true"> 
              </div>
            </div>
            <button id="button" type="submit" name="submit" class="btn btn1">
            <span id="">SAVE</span>
            </button>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection