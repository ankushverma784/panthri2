@extends('layouts.admin')
@section('content')
<div class="container mmt mb-5">
   <div class="card update-form">
      <div class="card-header">
         <h3 class="text-center title-2 font-weight-bold"> Supplier Detail</h3>
      </div>
   </div>
   <div class="card">
      <form action="" method="POST">
         <div class="forms bg-white p-4 my-3" id="cust-form">
            <div class="form-row mt-2">
               <div class="form-group col-md-6">
                  <label for="inputName">Mobile Number</label>
                  <input type="number" class="form-control" id="inputName" placeholder="Enter Mobile" name="mobile" required="true" onchange="checkCustomer(this.value)">
               </div>
               <div class="form-group col-md-6">
                  <label for="inputName">Supplier Name</label>
                  <input type="text" class="form-control" id="inputName" placeholder="Full Name" name="name" required="true">
               </div>
            </div>
            <div class="form-row">
               <div class="form-group col-md-6">
                  <label for="inputAmount">City</label>
                  <input type="text" class="form-control" id="inputAmount" placeholder="Enter City" name="city" required="true">
               </div>
               <div class="form-group col-md-6">
                  <label for="inputPolicyNo">State</label>
                  <input type="text" class="form-control" id="inputPolicyNo" placeholder="" value="Uttarakhand" name="state" required="true" disabled="">
               </div>
            </div>
            <div class="form-row">
               <div class="form-group col-md-12">
                  <label for="inputPolicyName">Address</label>
                  <input type="text" class="form-control" id="inputPolicyName" placeholder="Enter Address" name="address" required="true">
               </div>
            </div>
         </div>
         <div class="card-header">
            <h3 class="text-center title-2 font-weight-bold">Bill No:</h3>
         </div>
         <div class="form-row p-5">
            <div class="col-md-12">
               <div class="input-group input-icon right">
                  <span class="input-group-addon">
                  <i class="fa fa-rupee"></i>
                  </span>
                  <input id="text" class="form-control" type="text" placeholder="Enter Bill No" name="bill_no" required="">
               </div>
            </div>
         </div>
         <div class="forms mb-5 p-2">
            <div class="card-header mb-2">
               <h3 class="text-center title-2 font-weight-bold">Material Detail</h3>
            </div>
            <div id="input_fields_wrap">
               <div class="row pt-4" id="material-row">
                  <div class="col-md-11">
                     <div class="row">
                        <div class="col-lg-2">
                           <select class="custom-select custom-select-md" name="material-name[]" onchange="get(this.value,this.id)" id="0">
                              <option selected="" disabled="">Select Material</option>
                              <option value="AGGREGATE">Aggregate</option>
                              <option value="CEMENT">Cement</option>
                              <option value="BRICK">Brick</option>
                              <option value="SAND">Sand</option>
                           </select>
                        </div>
                        <div class="col-lg-2">
                           <select class="custom-select custom-select-md" name="material-type[]" placeholder="Material Type" id="mt0">
                              <option selected="" disabled="">Material Type</option>
                           </select>
                        </div>
                        <div class="col-lg-2">
                           <select class="custom-select custom-select-md" name="material-quantity-type[]">
                              <option selected="" disabled="">Quantity Type</option>
                              <option value="Bag">Bag</option>
                              <option value="Pieces">Pieces</option>
                              <option value="Quantel">Quantel</option>
                           </select>
                        </div>
                        <div class="col-lg-1 rate-input">
                           <input id="quantity0" class="form-control" type="number" placeholder="Qty" name="material-quantity[]" required="" onchange="getTotalPrice(0);calculateTotal();" autocomplete="off">
                        </div>
                        <div class="col-lg-1 rate-input">
                           <input id="price0" class="form-control" type="number" placeholder="Price" name="material-price[]" required="" onkeyup="getTotalPrice(0);calculateTotal();" autocomplete="off">
                        </div>
                        <div class="col-lg-1 rate-input">
                           <input class="form-control" type="text" placeholder="0%" name="material-gst[]" required="" id="gst0">
                        </div>
                        <div class="col-lg-1 rate-input">
                           <input id="tax0" class="form-control" type="number" placeholder="Tax" name="material-tax[]" required="" readonly="">
                        </div>
                        <div class="col-lg-2 rate-input">
                           <input id="total-price0" class="form-control" type="number" placeholder="Total Price" name="material-total-price[]" required="" readonly="">
                        </div>
                     </div>
                  </div>
                  <div class="col-md-1 rate-btn">
                     <button type="button" class="btn btn1" id="add_field_button">+</button>
                  </div>
               </div>
            </div>
            <hr>
            <div class="form-group mt-4">
               <div class="row">
                  <div class="col-sm-3">
                     <label>Delivery Charges :</label>
                     <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="isdeliveryCharge" id="yes" value="yes" checked="">
                        <label class="form-check-label" for="inlineRadio1">Yes</label>
                     </div>
                     <div class="form-check form-check-inline">
                        <input class="form-check-input" type="radio" name="isdeliveryCharge" id="no" value="no" >
                        <label class="form-check-label" for="inlineRadio2">No</label>
                     </div>
                  </div>
                  <div class="col-sm-2">
                     <input id="deliveryCharge" class="form-control" type="number" placeholder="0" name="deliveryCharge" required="" oninput="finalPrice()" autocomplete="off">
                  </div>
                  <label class="col-sm-2 text-md-right my-auto my-md-1">Other Charges</label>
                  <div class="col-sm-1 p-0">
                     <input id="otherCharge" class="form-control" type="number" placeholder="0" name="otherCharge" required="" oninput="finalPrice()" autocomplete="off">
                  </div>
                  <label class="col-sm-2 text-md-right my-auto my-md-1">Loading Charges</label>
                  <div class="col-sm-2">
                     <input id="loadingCharge" class="form-control" type="number" placeholder="0" name="loadingCharge" required="" oninput="finalPrice()" autocomplete="off">
                  </div>
               </div>
            </div>
            <hr class="my-3">
            <table class="table table-responsive-md table-bordered mb-5">
               <thead>
                  <tr class="btn1 text-white text-center">
                     <th scope="col">Total Price</th>
                     <th scope="col">Tax</th>
                     <th scope="col">Delivery Charges</th>
                     <th scope="col">Other Charges</th>
                     <th scope="col">Loading Charges</th>
                     <th scope="col">Grand Total</th>
                  </tr>
               </thead>
               <tbody class="text-center" id="sale-prices">
                  <tr>
                     <td><input id="finalprice" class="form-control" type="number" placeholder="Rs.0" name="finalprice" readonly=""></td>
                     <td><input id="finaltax" class="form-control" type="number" placeholder="Rs.0" name="finaltax" readonly=""></td>
                     <td><input id="finaldcharge" class="form-control" type="number" placeholder="Rs.0" name="finaldcharge" readonly=""></td>
                     <td><input id="finalocharge" class="form-control" type="number" placeholder="Rs.0" name="finalocharge" readonly=""></td>
                     <td><input id="finallcharge" class="form-control" type="number" placeholder="Rs.0" name="finallcharge" readonly=""></td>
                     <td><input id="grandtotal" class="form-control" type="number" placeholder="Rs.0" name="grandtotal" readonly=""></td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div class="forms mb-4">
            <div class="card-header mb-2">
               <h3 class="text-center title-2 font-weight-bold">Payment Detail</h3>
            </div>
            <div class="row p-2">
               <div class="col-sm-12">
                  <label>Payment Status : </label>
                  <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="payment-status" id="inlineRadio1" value="Paid">
                     <label class="form-check-label" for="inlineRadio1">Paid</label>
                  </div>
                  <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="payment-status" id="inlineRadio2" value="UnPaid">
                     <label class="form-check-label" for="inlineRadio2">UnPaid</label>
                  </div>
               </div>
            </div>
            <div class="row p-2">
               <div class="col-sm-12">
                  <label>Payment Type : </label>
                  <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="payment-type" id="inlineRadio1" value="Cash">
                     <label class="form-check-label" for="inlineRadio1">Cash</label>
                  </div>
                  <div class="form-check form-check-inline">
                     <input class="form-check-input" type="radio" name="payment-type" id="inlineRadio2" value="Cheque">
                     <label class="form-check-label" for="inlineRadio2">Cheque</label>
                  </div>
               </div>
            </div>
            <div class="form-row align-items-center p-2 pb-5">
               <div class="col-md-6">
                  <label class="mr-sm-2" for="inlineFormCustomSelect">Paid Amount</label>
                  <input id="email" type="text" class="form-control" name="payment-amount" placeholder="Enter Amount">
               </div>
               <div class="col-md-6">
                  <label class="mr-sm-2" for="inlineFormCustomSelect">Cheque No</label>
                  <input id="email" type="text" class="form-control" name="payment-cheque" placeholder="Enter Cheque No">
               </div>
            </div>
         </div>
         <div class="row mb-5">
            <div class="form-group m-auto">
               <button type="submit" class="btn btn1 text-white" name="submit">Submit</button>
               <button class="btn btn1 text-white ml-2" type="reset">Reset</button>
            </div>
         </div>
      </form>
   </div>
</div>
@endsection