@extends('layouts.admin')
@section('content')
<div class="container mmt mb-5">
  
  <!-- DATA TABLE -->
  <div class="card shadow mb-4 datatable-card" id="card">
    <div class="card-header py-3">
      <h3 class="mb-4 font-weight-bold ml-sm-1">List Of Customer</h3>
      <h4 class="badge-danger text-center" id="delete_msg"></h4>
      <form class="form-inline customer-form" name="form" action="/admin/manage-agreement.php" method="POST">
        <label for="email">Customer Mobile:</label>
        <div class="input-group ml-lg-2 ml-md-2 ml-sm-1 my-2 number-input">
          <span class="input-group-addon align-items-center d-flex pr-2"><i class="fa fa-user"></i></span>
          <input id="put-mobile-no" type="number" class="form-control" name="phone" placeholder="Enter Mobile No" onkeyup="getNumber(this.value)" autocomplete="off" required="true">
          <ul class="get-number" id="get-number"></ul>
        </div>
        <button type="submit" id="submit" name="submit" class="btn btn1 ml-lg-3 ml-md-3 ml-sm-1 search-button">Search</button>
      </form>
    </div>
    <div class="card-body" id="card-body">
      <div class="table-responsive">
        <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
          <div class="row">
            <div class="col-sm-12 col-md-6">
              <div class="dataTables_length" id="dataTable_length">
                <label>
                  Show 
                  <select name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
                    <option value="10">10</option>
                    <option value="25">25</option>
                    <option value="50">50</option>
                    <option value="100">100</option>
                  </select>
                  entries
                </label>
              </div>
            </div>
            <div class="col-sm-12 col-md-6">
              <div id="dataTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="dataTable"></label></div>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12">
              <table class="table table-bordered text-center dataTable no-footer" id="dataTable" role="grid" aria-describedby="dataTable_info" style="width: 100%;" width="100%" cellspacing="0">
                <thead class="btn1"> 
                  <tr role="row">
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 104.55px;" aria-label="Phone No: activate to sort column ascending">Phone No</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 176.1px;" aria-label="Customer Name: activate to sort column ascending">Customer Name</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 95.6333px;" aria-label="City: activate to sort column ascending">City</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 95.2333px;" aria-sort="ascending" aria-label="Pan : activate to sort column descending">Pan No</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 111.033px;" aria-label="Gst No: activate to sort column ascending">Gst No</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 93.5px;" aria-label="Address: activate to sort column ascending">Address</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 47.5833px;" aria-label="Adhar: activate to sort column ascending">Aadhar</th>
                    <th tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" style="width: 74.3667px;" aria-label="Delete: activate to sort column ascending">Action</th>
                  </tr>
                </thead>
                <tbody id="table-body">
                  <!-- SEARCH CUSTOMER TABLE-->
                  <!-- ALL CUSTOMERS TABLE-->
                  <tr role="row" class="odd">
                    <td class="sorting_1">2020-10-09</td>
                    <td>7055572245</td>
                    <td>Ankit Singh</td>
                    <td>dehradun 1</td>
                    <td>Uttarakhand</td>
                    <td>Dehradun</td>
                    <td><a href="edit-agreement.php?data=1"><i class="fa fa-edit font-aw"></i></a></td>
                    <td><a href="delete.php?agree_id=1"><i class="fas fa-trash-alt"></i></a></td>
                  </tr>
                </tbody>
              </table>
            </div>
          </div>
          <div class="row">
            <div class="col-sm-12 col-md-5">
              <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div>
            </div>
            <div class="col-sm-12 col-md-7">
              <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                <ul class="pagination">
                  <li class="paginate_button page-item previous disabled" id="dataTable_previous"><a href="#" aria-controls="dataTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                  <li class="paginate_button page-item active"><a href="#" aria-controls="dataTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                  <li class="paginate_button page-item next disabled" id="dataTable_next"><a href="#" aria-controls="dataTable" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  <!-- END DATA TABLE -->
</div>
</div>
@endsection