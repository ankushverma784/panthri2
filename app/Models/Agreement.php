<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Agreement extends Model
{
    use HasFactory;
    protected $fillable = ['mobile', 'name','city','address','selectMaterial','materialType','quantityType','minRate','maxRate'];
}
