<?php

namespace App\Http\Controllers;
use App\Models\Purchase;
use Illuminate\Http\Request;

class PurchaseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $purchase = Purchase::all();

        return view ('admin/purchase.index',compact('purchase'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view ('admin/purchase.create');
        // return Response ::json (array()); 
    }
    public function transaction()
    {
        //
        return view ('admin/purchase.transaction');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       $data= $request->validate([
            'mobile' => 'required',  
            'name' => 'required',
            'city' => 'required',
            'address' => 'required',
            'state' => '',
        ]);   
        // $purchase = Purchase::create($data);
        // return Purchase::json($purchase);
        


        Purchase::create($request->all());
     
        return redirect()->route('admin.purchase.index')
                        ->with('success','Suplier Details Added successfully.');
    
                    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        return view ('admin/purchase.transaction');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $req)
    {
        $data = Data::find($req->id);
        // $data->name = $req->name;
        // $data->save();

        // return response()->json($data);
        return view('data.edit', compact('userData','id'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $name = $request->input('name');
        $address = $request->input('address');
        $editid = $request->input('editid');
    
        if($name !='' && $address != ''){
          $data = array('name'=>$name,"address"=>$address);
    
          // Call updateData() method of Page Model
          Page::updateData($editid, $data);
          echo 'Update successfully.';
        }else{
          echo 'Fill all fields.';
        }
    
        exit; 

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)



{

}
    
    // {
    //     $item = Purchase::findorFail($id);
    //         if($item->delete())
    //         {
    //         return redirect('admin/purchase/index'. $this->purchase)->with('success', trans('app.' . $this->purchase . '.destroy_success'));
    //     }
    //     return redirect('admin/purchase/index'. $this->purchase)->with('error', trans('app.' . $this->purchase . '.destroy_fail'));
    // }







    // {
    //     return redirect()->route('admin.purchase.index')
    //     ->with('success','Suplier deleted successfully');
    // }
    public function managePurchase()
    {
        return view ('admin/purchase.managePurchase');
    }
    public function purchaseMaterial()
    {
        return view ('admin/purchase.purchaseMaterial');
    }
    public function ajaxView(Request $request)
    {
        // $data = $request();
        

        $data = $request->all();

        // return response()
        // ->json($data);
        return Request::ajax() ? 
        response()->json($purchase,Response::HTTP_OK) 
        : abort(404);
    }

}

